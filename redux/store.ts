import { combineReducers, createStore } from 'redux';

import user, { initialUser } from './User/user';

const combinedReducer = combineReducers({
    user
})

const store = createStore(combinedReducer);

export default store;

export type StoreStateType = ReturnType<typeof store.getState>;
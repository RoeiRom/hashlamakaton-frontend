import User from '../../models/User';

import { userAction, SET_USER } from './userActions';

export const setUser = (userToSet: User): userAction => ({
    type: SET_USER,
    payload: {
        user: userToSet
    }
});
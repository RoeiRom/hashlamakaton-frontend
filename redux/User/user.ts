import User from '../../models/User';

import * as Actions from './userActions';

export const initialUser: User = {
    address: '',
    age: 0,
    bio: '',
    email: '',
    firstName: '',
    gender: false,
    id: 0,
    lastName: '',
    username: ''
};


const userReducer = (state = initialUser, action: Actions.userAction) : User => {
    switch (action.type) {
        case Actions.SET_USER: {
            return { ...state, ...action.payload.user }
        }
        default:  return state;
    }
}

export default userReducer;
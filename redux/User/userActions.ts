import User from '../../models/User';

export const SET_USER = 'SET_User';

interface SetUser {
    type: typeof SET_USER;
    payload: { user: User };
}

export type userAction = SetUser;
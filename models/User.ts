interface User {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    age: number;
    address: string;
    bio: string;
    gender: boolean;
    id: number;
}

export default User;
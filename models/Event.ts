interface Event {
    name: string;
    description: string;
    hostName: string;
    phone: string;
    link: string;
    averageAge: number;
    image: string;
}

export default Event;
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { useDispatch } from 'react-redux';

import { setUser } from '../../redux/User/userDispatches';

import axios from '../../utils/customAxios';

interface UseLoginIncome {
    username: string;
    password: string;
    navigation: NativeStackNavigationProp<any>;
}

const useLogin = ({ username, password, navigation }: UseLoginIncome) => {

    const dispatch = useDispatch();

    const onLoginClick = () => {
        axios.post('/login', { username, password })
            .then(response => {
                const { data: { user_id } } = response;
                axios.get(`/users/${user_id}`).then(userResponse => {
                    const { first_name, last_name, ...rest } = userResponse.data;
                    dispatch(setUser({
                        firstName: first_name,
                        lastName: last_name,
                        ...rest
                    }));
                    navigation.navigate('HomeScreen')
                })
            })
    }

    return {
        onLoginClick
    };
};

export default useLogin;
import React, { useState } from "react";
import { View, Image } from "react-native";
import { Input, Button, Text } from "@rneui/base";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

import styles from "./LoginStyles";
import useLogin from "./useLogin";

const Login = ({
  navigation,
}: {
  navigation: NativeStackNavigationProp<any>;
}) => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const { onLoginClick } = useLogin({ username, password, navigation });

  return (
    <View style={styles.container}>
      <Image source={require("../../assets/logo.jpeg")} style={styles.image} />
      <Input value={username} onChangeText={setUsername} label="User name" />
      <Input
        value={password}
        onChangeText={setPassword}
        label="Password"
        secureTextEntry={true}
      />
      <Button onPress={() => navigation.navigate("SignUp")}>Sign Up</Button>
      <Button onPress={onLoginClick} style={{ margin: 10 }}>
        Login
      </Button>
    </View>
  );
};

export default Login;

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    image: {
        width: '80%',
        resizeMode: 'contain'
    },
    signUpLink: {
        color: 'blue',
        textDecorationLine: 'underline',
        marginBottom: '2%',
        fontSize: 20
    }
});

export default styles;
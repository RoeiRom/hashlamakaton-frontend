import React, { useState } from "react";
import MapView, { Marker } from "react-native-maps";
import { View, Dimensions } from "react-native";

const Locations = () => {
  const initialRegion = {
    latitude: 31.988,
    longitude: 34.83,
    latitudeDelta: 0.2022,
    longitudeDelta: 0.2021,
  };
  const [markers, changeMarkers] = useState([
    {
      title: "חוג א",
      description: "דוגמה",
      latitude: 31.981,
      longtitude: 34.777,
    },
    {
      title: "חוג ב",
      description: "דוגמה",
      latitude: 31.993,
      longtitude: 34.882,
    },
  ]);

  return (
    <View>
      <MapView
        region={initialRegion}
        style={{
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height,
        }}
      >
        {markers.map((marker, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longtitude,
            }}
            title={marker.title}
            description={marker.description}
          />
        ))}
      </MapView>
    </View>
  );
};

export default Locations;

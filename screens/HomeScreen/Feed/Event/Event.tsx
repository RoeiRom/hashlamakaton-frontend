import React from "react";
import { Card, Text } from "@rneui/base";

import styles from "./EventStyles";
import Event from "../../../../models/Event";

const Post = ({ event }: Props) => {
  return (
    <Card containerStyle={styles.card}>
      <Card.Title style={{ fontSize: 28, paddingTop: 14 }}>
        {event.name}
      </Card.Title>
      <Card.Image source={{ uri: `data:image/jpeg;base64,${event.image}` }} />
    </Card>
  );
};

interface Props {
  event: Event;
}

export default Post;

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    card: {
        width: '90%',
        padding: 0,
        borderRadius: 10
    }
});

export default styles;
import React, { useEffect, useState } from 'react';
import Event from '../../../models/Event';

import axios from '../../../utils/customAxios';

interface UseFeedIncome {
    events: Event[];
    setEvents: React.Dispatch<React.SetStateAction<Event[]>>;
}

const useFeed = ({ events, setEvents }: UseFeedIncome) => {

    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        axios.get('/activities')
            .then(response => {
                const events: any[] = response.data;
                setEvents(events.map(event => {
                    const { host_name, average_age, ...rest } = event;
                    return ({
                        hostName: host_name,
                        averageAge: average_age,
                        ...rest
                    });    
                }));
            })
            .finally(() => setLoading(false));
    }, []);

    return {
        loading
    }
};

export default useFeed;
import React, { useState } from "react";
import { Button, Text } from "@rneui/base";
import { ActivityIndicator, View, ScrollView } from "react-native";

import styles from "./FeedStyle";
import Post from "./Event/Event";
import Event from "../../../models/Event";
import useFeed from "./useFeed";

const Feed = () => {
  const [events, setEvents] = useState<Event[]>([]);

  const { loading } = useFeed({ events, setEvents });

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" />
      ) : (
        <ScrollView
          style={{ width: "100%" }}
          contentContainerStyle={{ display: "flex", justifyContent: "center" }}
        >
          {events.map((event, index) => (
            <Post event={event} key={index} />
          ))}
        </ScrollView>
      )}
    </View>
  );
};

export default Feed;

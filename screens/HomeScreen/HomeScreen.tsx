import React from "react";
import { View } from "react-native";
import { Text, Header, Icon } from "@rneui/base";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Feed from "./Feed/Feed";
import Profile from "./Profile/Profile";
import Locations from "./Locations/Locations";

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
  return (
    <>
      <Header centerComponent={{ text: "Header" }} />
      <Tab.Navigator screenOptions={{ headerShown: false }}>
        <Tab.Screen
          name="Feed"
          component={Feed}
          options={{
            tabBarIcon: ({ focused, color, size }) => (
              <Icon name="forum" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Locate"
          component={Locations}
          options={{
            tabBarIcon: ({ focused, color, size }) => (
              <Icon name="place" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default HomeScreen;

import React, { useEffect, useState } from "react";
import { View, Text, Alert } from "react-native";
import SignUpChips from "./SignUpChips";

const SignUpChipsGroup = ({ chipsData, onChange }) => {
  const [list, changeList] = useState([]);

  const onChipChange = (data, index) => {
    changeList((prevState) => {
      const newState = [...prevState];
      newState[index] = data;
      return newState;
    });
  };

  useEffect(() => {
    const flattenlist = [].concat.apply([], list);
    onChange(flattenlist);
  }, [list]);

  return (
    <View>
      {chipsData.map((chip: string, index: number) => (
        <SignUpChips {...chip} onChange={(data) => onChipChange(data, index)} />
      ))}
    </View>
  );
};
export default SignUpChipsGroup;

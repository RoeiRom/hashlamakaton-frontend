import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { ButtonGroup, Icon, Text } from "@rneui/base";
import { ProgressSteps, ProgressStep } from "react-native-progress-steps";
import { useForm, Controller } from "react-hook-form";

import SignUpField from "./SignUpField";
import styles from "./SignUpStyles";
import axios from "../../utils/customAxios";
import SignUpChipsGroup from "./SignUpChipsGroup";

interface FieldType {
  name: string;
  label: string;
  type: "simple" | "password" | "email";
  hint: string;
}

const SignUp = ({ navigation }) => {
  const { control, handleSubmit, getValues } = useForm();
  const [selectedGender, changeGender] = useState<number>(-1);
  const [chipsData, changeChips] = useState([]);
  const basicFields: Array<FieldType> = [
    {
      name: "username",
      label: "שם משתמש",
      type: "simple",
      hint: "איך יקראו לך ביישמון",
    },
    {
      name: "password",
      label: "סיסמה",
      type: "password",
      hint: "לכניסות הבאות שלך",
    },
    {
      name: "confirm",
      label: "הכנס סיסמה בשנית",
      type: "password",
      hint: "נוודא שהזנת נכון",
    },
    {
      name: "email",
      label: "דואר אלקטרוני",
      type: "simple",
      hint: "נוודא שאתה אמיתי",
    },
    {
      name: "address",
      label: "כתובת",
      type: "simple",
      hint: "נקשר אותך לחוגים קרובים",
    },
  ];

  const childFields: Array<FieldType> = [
    {
      name: "name",
      label: selectedGender === 0 ? "מה שם הילד?" : "מה שם הילדה?",
      type: "simple",
      hint: "שנכיר אותה טוב יותר",
    },
    {
      name: "age",
      label: selectedGender === 0 ? "בן כמה הילד?" : "בת כמה הילדה?",
      type: "simple",
      hint: "בשביל חוגים רלוונטיים יותר",
    },
  ];

  useEffect(() => {
    axios.get("/tags").then((res) => changeChips(res.data));
  }, []);

  const onSubmit = (data) => {
    const formatData = {
      ...data,
      gender: !!data?.gender,
      age: Number(data?.age),
    };
    delete formatData?.confirm;
    axios.post("/users", formatData);
    navigation.navigate("HomeScreen");
  };

  return (
    <View style={styles.container}>
      <ProgressSteps>
        <ProgressStep label="הרשמת הורה" nextBtnText="המשך">
          {basicFields.map((struc: FieldType) => (
            <Controller
              control={control}
              name={struc.name}
              render={({ field: { onChange, value } }) => (
                <SignUpField
                  label={struc.label}
                  type={struc.type}
                  onChange={onChange}
                  placeholder={struc.hint}
                  value={value}
                />
              )}
            />
          ))}
        </ProgressStep>
        <ProgressStep
          label="על הילד\ה"
          previousBtnText="הקודם"
          nextBtnText="הבא"
        >
          <View>
            <Controller
              control={control}
              name="gender"
              render={({ field: { onChange } }) => (
                <ButtonGroup
                  selectedIndex={selectedGender}
                  onPress={(selectedIndex) => {
                    changeGender(selectedIndex);
                    onChange(selectedIndex);
                  }}
                  containerStyle={{
                    marginTop: 20,
                    height: 100,
                    backgroundColor: "inherit",
                    borderRadius: 20,
                  }}
                  selectedButtonStyle={{
                    backgroundColor: selectedGender === 1 ? "pink" : "#b3e6ff",
                  }}
                  buttons={[
                    {
                      element: () => (
                        <Icon name="man-outline" type="ionicon" size="80" />
                      ),
                    },
                    {
                      element: () => (
                        <Icon name="woman-outline" type="ionicon" size="80" />
                      ),
                    },
                  ]}
                />
              )}
            />
            {selectedGender >= 0 &&
              childFields.map((struc: FieldType) => (
                <Controller
                  control={control}
                  name={struc.name}
                  render={({ field: { onChange, value } }) => (
                    <SignUpField
                      label={struc.label}
                      type={struc.type}
                      onChange={onChange}
                      placeholder={struc.hint}
                      value={value}
                    />
                  )}
                />
              ))}
          </View>
        </ProgressStep>
        <ProgressStep
          label="הייחוד"
          finishBtnText="סיימתי"
          previousBtnText="הקודם"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Controller
            control={control}
            name="tags"
            render={({ field: { onChange } }) => (
              <SignUpChipsGroup chipsData={chipsData} onChange={onChange} />
            )}
          />
        </ProgressStep>
      </ProgressSteps>
    </View>
  );
};

export default SignUp;

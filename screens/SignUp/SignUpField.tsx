import React from "react";
import { View } from "react-native";
import { Input, Text } from "@rneui/base";

import styles from "./SignUpStyles";

interface FieldProps {
  label: string;
  type?: "simple" | "password" | "email";
  onChange: (value: string) => void;
  placeholder: string;
  value: string;
}

const SignUpField = (props: FieldProps) => {
  return (
    <View>
      <Text style={styles.label}>{props.label}</Text>
      <Input
        secureTextEntry={props.type === "password"}
        onChangeText={props.onChange}
        placeholder={props.placeholder}
        style={styles.input}
        value={props.value}
      />
    </View>
  );
};

export default SignUpField;

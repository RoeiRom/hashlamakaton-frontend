import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { Text, Chip } from "@rneui/base";
import styles from "./SignUpStyles";

interface ChipsProps {
  category: string;
  tags: Array<string>;
  onChange: (values: Array<String>) => void;
  value: Array<string>;
}

const SignUpChips = (props: ChipsProps) => {
  const [selectedChips, changeSelected] = useState<Array<string>>([]);

  const onChipSelect = (value: string): void => {
    if (selectedChips.includes(value)) {
      changeSelected((prevState) => prevState.filter((e) => e !== value));
    } else {
      changeSelected((prevState) => [...prevState, value]);
    }
  };

  useEffect(() => {
    props.onChange(selectedChips);
  }, [selectedChips]);

  return (
    <View>
      <Text style={styles.label}>{props.category}</Text>
      <View style={styles.chipContainer}>
        {props.tags.map((chip) => (
          <Chip
            title={chip}
            containerStyle={styles.chip}
            type={selectedChips.includes(chip) ? "solid" : "outline"}
            onPress={() => onChipSelect(chip)}
          />
        ))}
      </View>
    </View>
  );
};

export default SignUpChips;

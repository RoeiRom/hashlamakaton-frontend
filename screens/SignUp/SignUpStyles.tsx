import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    padding: 20,
    marginTop: 30,
    flex: 1,
    writingDirection: "rtl",
  },
  label: {
    fontFamily: "Arial Hebrew",
    fontWeight: "bold",
    fontSize: 20,
    marginLeft: 10,
    marginTop: 20,
    textAlign: "left",
  },
  input: {
    textAlign: "right",
  },
  chip: {
    margin: 5,
  },
  chipContainer: {
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
});
